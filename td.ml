let js_bot = false;;
(*
let js_bot = true;;
*)

type direction = [ `Up | `Down  | `Left | `Right | `Stop ] ;;

type game_info =
 {
   mutable timebank : int;
   mutable time_per_move : int;
   mutable player_names : string list;
   mutable your_bot : string;
   mutable your_botid : int;
   mutable width : int;
   mutable height : int;
   mutable max_rounds : int;
 }
;;

type player =
 {
   id : int;
   mutable p_snippets : int;
   mutable para : bool;
   mutable has_weapon : bool;
   mutable p_row : int;
   mutable p_col : int;
 }
;;

type bug =
 {
   mutable b_row : int;
   mutable b_col : int;
   mutable dir : direction;
 }
;;

type cell =
 {
  mutable blocked : bool;
  mutable snippet : bool;
  mutable sword : bool;
  mutable player0 : bool;
  mutable player1 : bool;
 }
;;

type game_state =
 {
   mutable field : cell array array;
   mutable round : int;
   mutable last_update : float;
   mutable last_timebank : int;
   mutable player : player array;
   mutable snippets : (int * int) list;
   mutable weapons : (int * int) list;
   mutable bugs : bug list;
   setup : game_info;
 }
;;



(* Welcome to the OCaml Starter for Hackman on Riddles.io
 *)

open Td;;
open Debug;;

(*
*)
let get_time = ref (fun () -> Unix.gettimeofday ());;

(*
let get_time = ref (fun () -> 0.0);;
*)

let adjacent =
 [
  (0, 0, "pass");
  (-1, 0, "up");
  (1, 0, "down");
  (0, -1, "left");
  (0, 1, "right");
 ]
;;

(* input processing *)

let is_mine gstate i =
  i = gstate.setup.your_botid
;;

let uncomment s =
  try String.sub s 0 (String.index s '#')
  with Not_found -> s
;;

let new_cell () = 
 {
   blocked = false;
   snippet = false;
   sword = false;
   player0 = false;
   player1 = false;
 }
;;

let new_field width height =
  let v = Array.make_matrix height width 0 in
    Array.map (fun x -> Array.map (fun y -> new_cell()) x) v
;;

let new_player v =
 {
   id = v;
   p_snippets = 0;
   para = false;
   has_weapon = false;
   p_row = 0;
   p_col = 0;
 }
;;

let clear_gstate gstate =
 (
  gstate.field <- new_field gstate.setup.width gstate.setup.height;
  gstate.round <- 0;
  gstate.player <- [| new_player 0; new_player 1 |];
  gstate.snippets <- [];
  gstate.weapons <- [];
  gstate.bugs <- [];
 )
;;

(* tokenizer from rosetta code *)
let split_char sep str =
  let string_index_from i =
    try Some (String.index_from str i sep)
    with Not_found -> None
  in
  let rec aux i acc = match string_index_from i with
    | Some i' ->
        let w = String.sub str i (i' - i) in
        aux (succ i') (w::acc)
    | None ->
        let w = String.sub str i (String.length str - i) in
        List.rev (w::acc)
  in
  aux 0 []
;;

let wipe_cell cell =
  cell.blocked <- false;
  cell.snippet <- false;
  cell.sword <- false;
  cell.player0 <- false;
  cell.player1 <- false
;;

let new_bug row col =
 {
  b_row = row;
  b_col = col;
  dir = `Stop;
 }
;;

let read_map_string gstate row col s =
  let cell = gstate.field.(row).(col) in
  wipe_cell cell;
  String.iter (
    function
    | '0' -> 
      cell.player0 <- true;
      gstate.player.(0).p_row <- row;
      gstate.player.(0).p_col <- col;
    | '1' -> 
      cell.player1 <- true;
      gstate.player.(1).p_row <- row;
      gstate.player.(1).p_col <- col;
    | 'x' -> cell.blocked <- true
    | 'E' -> gstate.bugs <- (new_bug row col) :: gstate.bugs
    | 'W' ->
      cell.sword <- true;
      gstate.weapons <- (row, col) :: gstate.weapons
    | 'C' ->
      cell.snippet <- true;
      gstate.snippets <- (row, col) :: gstate.snippets
    | _ -> ()
  ) s
;;

let update_game_field (gstate:game_state) data =
   List.iteri (fun i (s:string) ->
      let row = i / gstate.setup.width in
      let col = i mod gstate.setup.width in
         read_map_string gstate row col s
   ) (split_char ',' data)
;;

let action_move bot gstate t2 =
  gstate.last_timebank <- (int_of_string t2);
  bot gstate
;;

let update_player p key v =
  match key with
  | "snippets" -> p.p_snippets <- int_of_string v
  | "has_weapon" -> p.has_weapon <- bool_of_string (String.lowercase v)
  | "is_paralyzed" -> p.para <- bool_of_string (String.lowercase v)
  | _ -> ()
;;

let four_token (gstate:game_state) key t1 t2 t3 =
   if (t3 = "") || (t2 = "") || (t1 = "") || (key = "")  then (
     debug ("four_token fail: " ^ key ^ " " ^ t1 ^ " " ^ t2 ^ " " ^ t3 ^ "\n")
   ) else (
   match key with
    | "update" ->
      begin match t1 with
       | "game" ->
         begin match t2 with
          | "round" -> 
            gstate.last_update <- !get_time();
            gstate.round <- int_of_string t3
          | "field" -> update_game_field gstate t3
          | _ -> ()
         end
       | "player0" -> update_player gstate.player.(0) t2 t3
       | "player1" -> update_player gstate.player.(1) t2 t3
       | _ -> ()
      end
    | _ -> ()
    )
;;

let three_token bot gstate key t1 t2 =
   if (t2 = "") || (t1 = "") || (key = "")  then ( 
     debug ("three_token fail: " ^ key ^ " " ^ t1 ^ " " ^ t2 ^ " " ^ "\n")
   ) else (
     match key with
      | "settings" -> 
        begin match t1 with
         | "timebank" -> gstate.setup.timebank <- int_of_string t2
         | "time_per_move" -> gstate.setup.time_per_move <- int_of_string t2
         | "player_names" -> gstate.setup.player_names <- split_char ',' t2 
         | "your_bot" -> gstate.setup.your_bot <- t2
         | "your_botid" -> gstate.setup.your_botid <- int_of_string t2
         | "field_width" ->
           gstate.setup.width <- int_of_string t2;
           if gstate.setup.height > 0 then clear_gstate gstate
         | "field_height" ->
           gstate.setup.height <- int_of_string t2;
           if gstate.setup.width > 0 then clear_gstate gstate
         | "max_rounds" -> gstate.setup.max_rounds <- int_of_string t2
         | _ -> ()
        end
      | "action" -> 
        begin match t1 with
         | "move" -> action_move bot gstate t2
         | _ -> ()
        end
      | _ -> ()
   )
;;

let process_line bot gstate line =
  let tokens = split_char ' ' (uncomment line) in
  match List.length tokens with
  | 4 -> four_token gstate (List.nth tokens 0) (List.nth tokens 1) (List.nth tokens 2) (List.nth tokens 3)
  | 3 -> three_token bot gstate (List.nth tokens 0) (List.nth tokens 1) (List.nth tokens 2)
  | _ -> debug ("Incorrect bot input: " ^ line ^ "\n")
;;

let read_lines bot gstate =
  while true do
    let line = read_line () in
      process_line bot gstate line;
  done
;;

(* End input section *)

(* output section *)

let issue_order (_, _, s) =
   Printf.printf "%s\n" s;
   flush stdout;
;;

let string_of_cell cell =
  if cell.player0 then "0"
  else if cell.player0 then "1"
  else if cell.sword then "W"
  else if cell.snippet then "C"
  else if cell.blocked then "x"
  else "."
;;

let debug_field gstate =
  Array.iter (fun row ->
    debug "\n";
    Array.iter (fun cell -> debug (string_of_cell cell)) row
  ) gstate.field;
  debug "\n";
;;

(* End output section *)

(* Utility functions *)

let random_from_list lst =
  let len = List.length lst in
    List.nth lst (Random.int len)
;;

let in_bounds gstate row col =
  (row >= 0) && (col >= 0)
  && 
  (row < gstate.setup.height) && (col < gstate.setup.width)
;;

let within_reach player row col =
  abs (player.p_row - row) + abs (player.p_col - col) < 2
;;

let is_legal gstate player row col =
  in_bounds gstate row col
  && not gstate.field.(row).(col).blocked
  && within_reach player row col
;;

let legal_moves gstate player =
  List.filter 
    (fun (row, col, str) -> 
      let result = is_legal gstate player row col in
        result
    )
    (List.map (fun (orow, ocol, str) ->
      (player.p_row + orow, player.p_col + ocol, str)
    ) adjacent)
;;

let time_elapsed_this_turn gstate =
  (!get_time() -. gstate.last_update) *. 1000.
;;

let time_remaining gstate =
  (float_of_int gstate.last_timebank -. time_elapsed_this_turn gstate)
;;

(* End utility *)

let init f_get_time =
  get_time := f_get_time;
  Random.self_init (); 
  let game_info =
     {
      timebank = 0;
      time_per_move = 0;
      player_names = [];
      your_bot = "";
      your_botid = -1;
      width = 0;
      height = 0;
      max_rounds = 0;
     }
  in
  let game_state =
     {
      field = [|[| |]|];
      round = 0;
      last_update = 0.0;
      last_timebank = 0;
      player = [| new_player 0; new_player 1 |];
      snippets = [];
      weapons = [];
      bugs = [];
      setup = game_info;
     }
  in
  game_state 
;;

let feed_bot bot gstate line =
  process_line bot gstate line;
;;

let run_bot bot =
  let game_state = init !get_time in

  begin try
   (
     read_lines bot game_state
   )
  with exc ->
   (
    debug (Printf.sprintf
       "Exception in turn %d :\n" game_state.round);
    debug (Printexc.to_string exc);
    raise exc
   )
  end;
;;



OCaml starter package for Hackman
=================================

see booking.riddles.io for details of the game
----------------------------------------------

The provided "main.ml" file is a minimal bot which chooses a random move from the list of legal moves.

As seen in that example, your bot should provide a function which takes a game_state as its argument, and calls issue_order once.

You then provide this function as an argument to the Hackman.run_bot function.

Have a look at td.ml to see the definition of the game_state data type.

There is a Bucklescript mode which allows the bot to be compiled to Javascript, but this is tricky and requires some complex local setup which I don't have time to document. The build script may give you some hints, or ask me if you want more detail provided on this.


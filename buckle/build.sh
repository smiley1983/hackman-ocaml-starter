#!/bin/bash
set -x
set -e
PATH=/home/smiley/build/bucklescript/jscomp/bin:/home/smiley/build/bucklescript/ocaml/bin:$PATH
BSC_LIB=./jscomp/stdlib
SOURCE=(td debug hackman main)
for i in "${SOURCE[@]}"
do
  cp ../"$i".ml .
done

for i in "${SOURCE[@]}"
do
  bsc -I . -I $BSC_LIB -c "$i".ml
done

for i in "${SOURCE[@]}"
do
  cat "$i".js | sed -e 's:..\/..\/..\/..\/..\/..\/build\/bucklescript\/:.\/:g' > "$i".js.js
  mv "$i".js.js "$i".js;
done

cp ../ProxyBot.js .

#cp ../td.ml ../debug.ml ../go.ml ../main.ml .
#for i in td.ml debug.ml go.ml main.ml; do bsc -I . -I $BSC_LIB -c "$i"; done
#for i in td.js debug.js go.js main.js; do cat "$i" | sed -e 's:..\/..\/..\/..\/..\/build\/bucklescript\/:.\/:g' > "$i".js; mv "$i".js "$i"; done 
#cp ../ProxyBot.js .

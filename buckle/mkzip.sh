#!/bin/bash
BASE=hackman_jsocaml
set -e
set -x
rm -f "$BASE.zip"
zip -r "$BASE.zip" td.js hackman.js main.js ProxyBot.js jscomp/*/*.js

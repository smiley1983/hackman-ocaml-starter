// __main__
const readline = require('readline');
const Bot = require('./hackman');
const Main = require('./main');

const io = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


var getTime = function () {
  var date = new Date();
  return date.getTime() / 1000;
}

var split = function (sep, str) {
  var result = str.split(String.fromCharCode(sep));
  console.log(str);
  console.log(sep);
  console.log(result);
  return result;
}

function run () {
  var gstate = Bot.init(getTime);
  io.on('line', (cmd) => {
    Bot.feed_bot(Main.main, gstate, cmd);
  });

  io.on('close', () => {
    process.exit(0);
  });
}

run ();


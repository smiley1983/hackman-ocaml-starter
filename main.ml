open Td;;

let main gstate =
  let moves = Hackman.legal_moves gstate gstate.player.(gstate.setup.your_botid)in
  let move = Hackman.random_from_list moves in
    Hackman.issue_order move
;;

if not Td.js_bot then
  Hackman.run_bot main

